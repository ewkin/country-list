import React, {useEffect, useState} from 'react';
import axios from "axios";

import './CountryInfo.css';


const CountryInfo = props => {
    const [countryInfo, setCountryInfo] = useState(null);

    useEffect(()=>{
        const  fetchData = async () =>{
            if(props.country){
                const countryInfoResponse = await axios.get('alpha/'+props.country);
                const promises = countryInfoResponse.data["borders"].map(async border =>{
                    const countryResponse = await axios.get('alpha/'+border);
                    return countryResponse.data['name'];
                });
                const newBorders = [await Promise.all(promises)];
                setCountryInfo({...countryInfoResponse.data, borders: newBorders});
            }
        };
        fetchData().catch(console.error);
    }, [props.country]);

    return countryInfo && (
        <div className="Countryinfo">
            <h1>{countryInfo.name}</h1>
            <img className="Countryflag" alt="flag" src={countryInfo.flag}/>
            <div><h4>Capital:</h4> {countryInfo['capital']}</div>
            <div><h4>Native Name:</h4>  {countryInfo['nativeName']}</div>
            <div> <h4>Borders with:</h4>
                {countryInfo.borders[0].map(border =>{
                    return <div key ={border}>{border}</div>
                })}
            </div>

        </div>
    );
};

export default CountryInfo;