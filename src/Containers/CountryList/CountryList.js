import React, {useEffect, useState} from 'react';
import axios from "axios";

import Country from "../../Components/Country/Country";
import CountryInfo from "../CountryInfo/CountryInfo";

import './CountryList.css';

const COUNTRY_URL='all?fields=name%3Balpha3Code';

const CountryList = () => {
    const [countryList, setCountryList] = useState([]);
    const [selectedCountry, setSelectedCountry] = useState(null);

    useEffect(()=>{
        const fetchData = async () => {
            const countryResponse = await axios.get(COUNTRY_URL);
            setCountryList(countryResponse.data);
        }
        fetchData().catch(console.error);
    }, []);

    return (
        <div className="Container">
            <div className="Countrylist">
            {countryList.map(country => (
                <Country
                    key={country['alpha3Code']}
                    id={country['alpha3Code']}
                    name ={country.name}
                    clicked ={setSelectedCountry}
                />
            ))}
        </div>
            <CountryInfo country ={selectedCountry}/>
        </div>
    );
};

export default CountryList;