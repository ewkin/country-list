import React from 'react';
import axios from "axios";

import CountryList from "./Containers/CountryList/CountryList";

axios.defaults.baseURL= 'https://restcountries.eu/rest/v2/';

const App = () => {
  return (
      <CountryList/>
  );
};

export default App;
