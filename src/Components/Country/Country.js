import React, {PureComponent} from 'react';
import './Country.css';


class Country extends PureComponent {

    render() {
        console.log('Country')
        return (
            <div className="Country" onClick={()=>this.props.clicked(this.props.id)}>{this.props.name}</div>
        );
    }
}

export default Country;